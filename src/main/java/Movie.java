import com.google.common.base.MoreObjects;

import java.time.LocalDate;

/**
 * Created by Apache on 16.08.2017.
 */
public class Movie {
    private String name;
    private MovieType type;
    private LocalDate releaseDate;
    private String author;

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
                .add("type", type)
                .add("releaseDate", releaseDate)
                .add("author", author)
                .toString();
    }

    public Movie(String name, MovieType type, LocalDate releaseDate, String author) {
        this.name = name;
        this.type = type;
        this.releaseDate = releaseDate;
        this.author = author;
    }

    public void setName(String name) {

        this.name = name;
    }

    public void setType(MovieType type) {
        this.type = type;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public MovieType getType() {
        return type;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public String getAuthor() {
        return author;
    }
}
