import java.util.HashMap;
import java.util.Map;

/**
 * Created by RENT on 2017-08-16.
 */
public class MovieDatabase {

    private Map<String,Movie> movies = new HashMap<>();

    public void addMovie(Movie movie){
        movies.put(movie.getName(),movie);
    }

    public Movie getMovie(String name){
        return movies.get(name);
    }

    public void printAllMovies(){
        for (Movie movie : movies.values()) {
            System.out.println(movie.toString());
        }

    }
    public void printAllMovies(MovieType type){
        //TODO
    }
}
