import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Created by RENT on 2017-08-16.
 */
public class Main {
    public static void main(String[] args) {
        MovieDatabase movieDB = new MovieDatabase();
        movieDB.addMovie(new Movie("Autopsja",MovieType.HORROR, LocalDate.now(),"Zenek"));
        movieDB.printAllMovies();
    }
}
