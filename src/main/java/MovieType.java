/**
 * Created by RENT on 2017-08-16.
 */
public enum MovieType {
    ACTION, DRAMA, COMEDY, HORROR
}
